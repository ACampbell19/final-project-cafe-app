Milestone Update:

During this time, i drafted a few layouts for the screens Main, View Menu, 
and Store information to get an idea of the path for my app. 
i also created a landscape variation for the launch screen to support rotation 
of the device.

I got one feature that uses an external device feature to work completely. 
By clicking the button "give us a call" on the "store information screen", it 
takes you outside of the app to the phone app to prepare for the call.
I started on creating my database to hold menu information and after writing 
all the inial code for the build, my app crashed. I have yet to find a solution 
of the problem so i converted the file to txt so ensure it didn`t interfere with 
my submission even though it is in progress. 

Final Update:

My app is Tiff`s Cafe. It has four primary functional screens that represent the 
features. It opens to the spalsh screen and the launches the home activity. 
from there you may proced to "Place an Order". This feature takes the user
to "Guest Information" and allows the user to enter information that is stored 
locally and can be recalled based on user preference. Before sending the user 
to view the menu. 
"Store information" is the next primary feature. It provides the user with a 
button "Find us" to show map with both the Cafe Address (TWU address was used)
and the users locations. It also gives the user an option "Give us a call" that
opens the pre-build phone service. 
"Leave a comment" is a way for the user to store comments in a txt file, while 
also being able to prompt the previous comment back into view from the file. 

All pages are supported in both vertical and horizonal views. 
The layouts are simple and not cluttered, with minimal color to avoid 
distraction of the app. 


Final Project

Project Description:
•	Myapp is a resource for a local coffee shop. My client is opening a store 
front café after testing out a food truck this past winter. She needs an app 
that can help cut down on in person orders, display menu items, prices, and 
description. 

What I propose to do is create an app that will do the following:
•	This app shall display the entire menu for café
•	This app shall be able to accept orders for the business 
•	This app shall accept customer information 
•	This app shall be able to accept comments from the user and stores the 
    users comment in a file
•	This app shall show user the café location and user location




I plan incorporate the following features:
•	GPS/Location Awareness – Once in “Store information”, the user will be 
able to see the cafes location including there own
•	Data Storage using key/value pair storage – the app will store user 
information on the app. 
•	Open Shared activity – After pressing the “give us the call”, they will be
redirected to  the open phone feature. 
•	Data storage using Core Data SQLite – The menu information will be stored 
in a separate database, so the owner only needs to edit the database not the 
back of the app.
•	Data storage using file read/write – The users comments will be stored in a
written into a file and then read out once prompted by the user.

Platform Justification:
I opted for using Android because there are quite a few more resources for 
android then iOS,

Major Features:
•	"Place an Order". This feature takes the user to "Guest Information" and 
allows the user to enter information that is stored locally and can be recalled
based on user preference. Before sending the user to view the menu. 
•	"Store information" is the next primary feature. It provides the user with 
a button "Find us" to show map with both the Cafe Address (TWU address was used)
and the users locations. It also gives the user an option "Give us a call" that
opens the pre-build phone service. 
•	"Leave a comment" is a way for the user to store comments in a txt file, 
while also being able to prompt the previous comment back into view from the 
file.

Testing Methodologies: 
Unfortunately given the current global circumstance, I was only able to text 
the app in the emulator. I did bring in family member to interact with it and 
give feedback. 

Usage:
Please run on Samsung Galaxy Tab 5e with Android 9. 
I noticed that running it a new device it did need time to full load because
some features were not loading. 





Lessons Learned:
I could write an entire paper about this project. The part that gave me the
most trouble and that I was not able to solve was the database. Most 
information was either overcomplicated with little explanation or some
methods/features had been deprecated. Considering that the database not 
working, I opted for completing an additional feature, but wanted to leave 
in the code to show an attempt at solving the issue. 

I also learned that you can not plan enough. It is important to know what
equipment you have access to, personal abilities when it comes to completing 
the task, and understanding the hierarchy of completion to efficiently work 
on the project give yourself enough time to debug. 

My partner dropped the class after having to homeschool her kid, and I should
of at least found a reliable source of help because building an app on your
own if not only difficult but also time consuming and exhausting. 

Finally, my most important lesson is how much I am capable of. I was working 
on the app for 6+ hours daily, researching, coding, debugging, problem solving,
redesigning on my own. Considering how hard I struggled in the beginning, I
think it is clear how hard I worked based on my success with this app, and 
the one unfortunate result. 





