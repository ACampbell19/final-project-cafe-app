package com.example.tiffs_cafe_final_project;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class HomeActivity extends AppCompatActivity {
  //  DatabaseHandler myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);



        //referencing buttons in the layout

            //VIEW BUTTON


        Button mainMenuBtn = findViewById(R.id.mainMenuBtn);

        mainMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent startViewMenu = new Intent(getApplicationContext(),ViewMenuActivity.class);
                startActivity(startViewMenu);

                //DatabaseHelper myDB = new DatabaseHelper(HomeActivity.this);
               // myDB.loadMainDatabase();


            }
        });

        //Place Order - customer information


        Button placeOrderBtn = findViewById(R.id.placeOrderBtn);

        placeOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent startCustomerInfo = new Intent(getApplicationContext(),GuestInformationActivity.class);
                startActivity(startCustomerInfo);
            }
        });

        Button commentBtn = findViewById(R.id.commentBtn);
        commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startComment = new Intent(getApplicationContext(),LeaveCommentActivity.class);
                startActivity(startComment);
            }
        });


        //view store information

        Button storeInfoBtn = findViewById(R.id.storeInfoBtn);
        storeInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startViewStore = new Intent(getApplicationContext(),StoreInfoActivity.class);
                startActivity(startViewStore);
            }
        });





    }



}

