
package com.example.tiffs_cafe_final_project;


public class MenuModel {


    //Declaring some variables
    private String itemId;
    private String menuId;
    private String itemDescription;
    private String itemPhoto;
    private int price;

    //constructors

    public MenuModel(String itemId, String menuId, String itemDescription, String itemPhoto, int price) {
        this.itemId = itemId;
        this.menuId = menuId;
        this.itemDescription = itemDescription;
        this.itemPhoto = itemPhoto;
        this.price = price;
    }

    public MenuModel() {
    }

 //setters and getters


    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }


    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getItemDescription() {
        return itemDescription;
    }


    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getItemPhoto() {
        return itemPhoto;
    }

    public void setItemPhoto(String itemPhoto) {
        this.itemPhoto = itemPhoto;
    }



    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}


