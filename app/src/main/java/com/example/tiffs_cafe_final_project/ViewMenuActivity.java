package com.example.tiffs_cafe_final_project;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class ViewMenuActivity extends AppCompatActivity {

    //DatabaseHelper myDB = new DatabaseHelper(ViewMenuActivity.this);
    //member variables

    //Buttons
    Button amMenuBtn;
    Button pmMenuBtn;
    Button coffeeMenuBtn;
    Button sweetsMenuBtn;
    Button viewCartBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_menu);

        amMenuBtn = findViewById(R.id.amMenuBtn);
        pmMenuBtn = findViewById(R.id.pmMenuBtn);
        coffeeMenuBtn = findViewById(R.id.coffeeMenuBtn);
        sweetsMenuBtn = findViewById(R.id.sweetsMenuBtn);
        viewCartBtn = findViewById(R.id.viewCartBtn);

        amMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startAmMenuBtn = new Intent(getApplicationContext(), breakfastOptiActivity.class);
                startActivity(startAmMenuBtn);


            }
        });

        pmMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startLunchMenu = new Intent(getApplicationContext(), LunchOptActivity.class);
                startActivity(startLunchMenu);
            }
        });

        coffeeMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startCoffeeMenu = new Intent(getApplicationContext(), DrinksOptActivity.class);
                startActivity(startCoffeeMenu);
            }
        });

        sweetsMenuBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startSweetsMenu = new Intent(getApplicationContext(), SweetsOptActivity.class);
                startActivity(startSweetsMenu);
            }
        });

        viewCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent viewCart = new Intent(getApplicationContext(), ViewCartActivity.class);
                startActivity(viewCart);
            }
        });


    }

}
