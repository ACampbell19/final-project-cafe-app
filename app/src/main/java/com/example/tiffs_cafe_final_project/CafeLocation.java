package com.example.tiffs_cafe_final_project;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class CafeLocation extends FragmentActivity{


    private GoogleMap mMap;
    FusedLocationProviderClient fusedLocationProviderClient;
    FusedLocationProviderClient user;
    SupportMapFragment mapFragment;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cafe_location);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        //mapFragment.getMapAsync(this);

        //initialize fusedLocationProvider client
        fusedLocationProviderClient = getFusedLocationProviderClient(this);
        user = LocationServices.getFusedLocationProviderClient(this);

        if(ActivityCompat.checkSelfPermission(CafeLocation.this,
                Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
            getCurrentLocation();

        } else {
            ActivityCompat.requestPermissions(CafeLocation.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        }



    }

    private void getCurrentLocation() {
        Task<Location> task = user.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                if(location != null){
                    //sync map
                    mapFragment.getMapAsync(new OnMapReadyCallback() {


                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            mMap = googleMap;
                            LatLng userLatLng = new LatLng(location.getLatitude(),
                                    location.getLongitude());
                            MarkerOptions userOptions = new MarkerOptions().position(userLatLng).title("you are here");
                            mMap.addMarker(userOptions);

                            LatLng cafe = new LatLng(33.2263, -97.1275);
                            mMap.addMarker(new MarkerOptions().position(cafe).title("Tiff`s Cafe"));
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(cafe));

                        }

                    });
                }
            }

    });
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    // private static final String TAG = "CafeLocation";
  /*  @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;



        // Add a marker of Cafe = Twu address and moves camera
        LatLng cafe = new LatLng(33.2263, -97.1275);
        mMap.addMarker(new MarkerOptions().position(cafe).title("Cafe Location"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(cafe));}
*/



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 44){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            getCurrentLocation();}
        }
    }

}




//end bracket




