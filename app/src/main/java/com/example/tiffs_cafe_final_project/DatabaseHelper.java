
package com.example.tiffs_cafe_final_project;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public abstract class DatabaseHelper extends SQLiteOpenHelper {

    //global variables

    public static final String MENU_TABLE = "MENU_TABLE";
    public static final String COLUMN_ITEM_ID = "ITEM_ID";
    public static final String COLUMN_MENU_ID = "MENU_ID";
    public static final String COLUMN_ITEM_DESCRIPTION = "ITEM_DESCRIPTION";
    public static final String COLUMN_PRICE = "PRICE";


    //constructor
    public DatabaseHelper(@Nullable Context context) {
        super(context, "cafe.db", null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //should be code to create a new table, called the first time the database is accessed

        //creating main menu table
        String createMenuTable = "CREATE TABLE IF NOT EXISTS " + MENU_TABLE + " (" + COLUMN_ITEM_ID + " TEXT, " + COLUMN_MENU_ID + " INTEGER, " + COLUMN_ITEM_DESCRIPTION + " TEXT, " + COLUMN_PRICE + " REAL)";
        db.execSQL(createMenuTable);

       // loadMenuOptions();
        // loadMainDatabase();


    }
    /*
   public abstract Resources getResources();
    private List<MenuModel> menuOptions = new ArrayList<>();




    @Override//version control
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS MENU_TABLE");
        onCreate(db);

    }

    protected void loadMenuOptions() {
        InputStream file;
        file = getResources().openRawResource(R.raw.mainmenu);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(file, Charset.forName("UTF-8"))
        );

        String dontKnow = "";
        try{
            while((dontKnow = reader.readLine()) != null){
                String[] tokens = dontKnow.split(",");

                //read the data
                MenuModel sample = new MenuModel();
                    sample.setItemId(tokens[0]);

            }
        } catch (IOException e){
            Log.wtf("my db", "error getting items" + dontKnow, e);
            e.printStackTrace();
        }

    }
    /*

    //add an activity that loads the main menu database
    public ArrayList<HashMap<String, String>> loadMainDatabase(){
        ArrayList<HashMap<String, String>> menuInfo;

        menuInfo = new ArrayList<>();
        String selectMenuQuery = "SELECT * FROM MENU_TABLE";
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(selectMenuQuery, null);
            if(cursor.moveToFirst()){
                do {
                    //itemId, menuId, ItemDescription, Item price
                    HashMap<String, String> map = new HashMap<String, String>();
                            map.put("itemId", cursor.getString(0));
                            map.put("menuId", cursor.getString(1));
                            map.put("itemDescription", cursor.getString(2));
                            map.put("itemPrice", cursor.getString(3));
                            menuInfo.add(map);

                }while (cursor.moveToNext());

                }
        return menuInfo;
    }





*/

}
