package com.example.tiffs_cafe_final_project;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class StoreInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_info);



        //call the store action
        Button numberBtn = findViewById(R.id.numberBtn);
       final Uri storeNumber = Uri.parse("tel:1234567890");
        numberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startCall = new Intent (Intent.ACTION_DIAL, storeNumber);
                startActivity(startCall);
            }
        });
        Button leaveCommentBtn = findViewById(R.id.leaveCommentBtn);
        leaveCommentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startCommentBtn = new Intent(getApplicationContext(),LeaveCommentActivity.class);
                startActivity(startCommentBtn);
            }
        });
        Button addressBtn = findViewById(R.id.addressBtn);
        addressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openMaps = new Intent (getApplicationContext(), CafeLocation.class);
                startActivity(openMaps);
            }
        });







    }
}
