package com.example.tiffs_cafe_final_project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class GuestInformationActivity extends AppCompatActivity {

    private SharedPreferences myPref;
    SharedPreferences.Editor prefEdit;
    EditText firstNameEdit, lastNameEdit, emailText, phoneText;
    CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest_information);

        //declaring all variables
        firstNameEdit = findViewById(R.id.firstNameEdit);
        lastNameEdit = findViewById(R.id.lastEditText);
        emailText = findViewById(R.id.emailText);
        phoneText = findViewById(R.id.phoneText);
        checkBox = findViewById(R.id.checkBox);
        Button buildOrderBtn = findViewById(R.id.buildOrderBtn);



        //myPref = getPreferences(Context.MODE_PRIVATE);
     myPref = getSharedPreferences("userInfo",MODE_PRIVATE);
      // myPref = PreferenceManager.getDefaultSharedPreferences(this);
       prefEdit = myPref.edit();

     checkSharedPreferences();


        //PLACE ORDER BUTTON




        buildOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (checkBox.isChecked()) {
                    prefEdit.putString(getString(R.string.checkbox), "True");
                    prefEdit.apply();

                    String firstname = firstNameEdit.getText().toString();
                    prefEdit.putString(getString(R.string.firstname), firstname);
                    prefEdit.apply();

                    String lastname = lastNameEdit.getText().toString();
                    prefEdit.putString(getString(R.string.lastname), lastname);
                    prefEdit.apply();

                    String email = emailText.getText().toString();
                    prefEdit.putString(getString(R.string.email), email);
                    prefEdit.apply();

                    String phone = phoneText.getText().toString();
                    prefEdit.putString(getString(R.string.phone), phone);
                    prefEdit.apply();

                    Toast.makeText(getApplicationContext(),"Information Saved", Toast.LENGTH_SHORT).show();
                } else {

                        prefEdit.putString(getString(R.string.checkbox), "False");
                        prefEdit.apply();


                        prefEdit.putString(getString(R.string.firstname), "");
                        prefEdit.apply();


                        prefEdit.putString(getString(R.string.lastname), "");
                        prefEdit.apply();


                        prefEdit.putString(getString(R.string.email), "");
                        prefEdit.apply();


                        prefEdit.putString(getString(R.string.phone), "");
                        prefEdit.apply();

                    }

               Intent startPlaceOrder = new Intent(getApplicationContext(), ViewMenuActivity.class);
                startActivity(startPlaceOrder);

            }


        });
    }

    private void checkSharedPreferences(){

        String checkbox = myPref.getString(getString(R.string.checkbox), "False");
        String firstname = myPref.getString(getString(R.string.firstname), " ");
        String lastname = myPref.getString(getString(R.string.lastname), " ");
        String email = myPref.getString(getString(R.string.email), " ");
        String phone = myPref.getString(getString(R.string.phone), " ");

        firstNameEdit.setText(firstname);
        lastNameEdit.setText(lastname);
        emailText.setText(email);
        phoneText.setText(phone);

        if (checkbox.equals("True")) {
            checkBox.setChecked(true);
        } else{
            checkBox.setChecked(false);
        }
    }




}
